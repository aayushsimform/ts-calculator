let display = document.getElementById("display");
let buttons = document.getElementsByClassName("button");
var arr = Array.prototype.slice.call(buttons);
arr.map((button) => {
    button.addEventListener("click", (e) => {
        switch (e.target.innerText) {
            case "C":
                display.innerText = " ";
                break;
            case "←":
                if (display.innerText)
                    display.innerText = display.innerText.slice(0, -1);
                break;
            case "π":
                display.innerText += "3.14";
                break;
            case "2nd":
                break;
            case "nd":
                break;
            case "x³":
                display.innerText = String(Math.pow(parseInt(display.innerText), 3));
                break;
            case "x²":
                display.innerText = String(Math.pow(parseInt(display.innerText), 2));
                break;
            case "3√x":
                display.innerText = String(Math.cbrt(parseInt(display.innerText)));
                break;
            case "2ˣ":
                display.innerText = String(Math.pow(2, parseInt(display.innerText)));
                break;
            case "log₂":
                display.innerText = String(Math.log2(parseInt(display.innerText)));
                break;
            case "eˣ":
                display.innerText = String(Math.pow(2.7182, parseInt(display.innerText)));
                break;
            case "2√x":
                display.innerText = String(Math.sqrt(parseInt(display.innerText)));
                break;
            case "x^y":
                display.innerText += "**";
                break;
            case "10ˣ":
                display.innerText = String(Math.pow(10, parseInt(display.innerText)));
                break;
            case "exp":
                display.innerText = String(Math.exp(parseInt(display.innerText)));
                break;
            case "1/x":
                display.innerText = String(1 / parseInt(display.innerText));
                break;
            case "log":
                display.innerText = String(Math.log(parseInt(display.innerText)));
                break;
            case "ln":
                display.innerText = String(Math.log(parseInt(display.innerText)));
                break;
            case "F-E":
                display.innerText = Number(display.innerText).toExponential();
                break;
            case "n!":
                fact();
                break;
            case "RAD":
                break;
            case "DEG":
                break;
            case "MS":
                break;
            case "M+":
                break;
            case "M-":
                break;
            case "MR":
                break;
            case "MC":
                break;
            case "√":
                display.innerText += String(Math.sqrt(parseInt(display.innerText)));
                break;
            case "+/-":
                let sign = Number(display.innerText) > 0
                    ? 1
                    : Number(display.innerText) < 0
                        ? -1
                        : 0;
                display.innerText = String(sign);
                break;
            case "e":
                display.innerText += "2.7182";
                break;
            case "|x|":
                display.innerText = String(0 - parseInt(display.innerText));
                break;
            case "=":
                try {
                    display.innerText = eval(display.innerText);
                }
                catch (_a) {
                    display.innerText = "Syntax Error!";
                }
                break;
            default:
                display.innerText += e.target.innerText;
        }
    });
});
var togg = true;
function update() {
    if (togg) {
        document.getElementById("x2").innerText = "x³";
        document.getElementById("sqrt").innerText = "3√x";
        document.getElementById("xraisey").innerText = "x^y";
        document.getElementById("10x").innerText = "2ˣ";
        document.getElementById("log").innerText = "log₂";
        document.getElementById("logn").innerText = "eˣ";
        document.getElementById("2nd").style.color = "black";
        document.getElementById("2nd").style.backgroundColor = "#a0b8ff";
        togg = !togg;
    }
    else {
        document.getElementById("x2").innerText = "x²";
        document.getElementById("sqrt").innerText = "2√x";
        document.getElementById("xraisey").innerText = "x^y";
        document.getElementById("10x").innerText = "10ˣ";
        document.getElementById("log").innerText = "log";
        document.getElementById("logn").innerText = "ln";
        document.getElementById("2nd").style.color = "black";
        document.getElementById("2nd").style.backgroundColor = "white";
        togg = !togg;
    }
}
var mem = [];
function memory(id) {
    switch (id) {
        case "MS":
            mem.unshift(Number(parseInt(display.innerText)));
            break;
        case "MC":
            mem = [];
            break;
        case "M+":
            if (mem[0])
                display.innerText = parseInt(display.innerText) + mem[0];
            break;
        case "M-":
            if (mem[0])
                display.innerText = String(mem[0] - Number(parseInt(display.innerText)));
            break;
        case "MR":
            if (mem[0])
                display.innerText += mem[0];
            break;
    }
    document.getElementById("memory").innerHTML =
        mem.length === 0 ? "empty memory" : mem[0];
}
function fact() {
    var v, res = 1;
    v = parseInt(display.innerText);
    for (let i = 1; i < v + 1; i++) {
        res = res * i;
    }
    display.innerText = String(res);
}
var radtodeg = true;
function deg() {
    if (radtodeg) {
        var dc = document.getElementById("change");
        dc.innerText = "DEG";
        dc.style.color = "black";
        dc.style.backgroundColor = "#a0b8ff";
        radtodeg = !radtodeg;
    }
    else {
        var dc = document.getElementById("change");
        dc.innerText = "RAD";
        dc.style.color = "black";
        dc.style.backgroundColor = "white";
        radtodeg = !radtodeg;
    }
}
var trig = document.getElementById("trig");
trig.onchange = () => {
    if (trig.value == "sin") {
        sine();
    }
    if (trig.value == "cos") {
        cosec();
    }
    if (trig.value == "tan") {
        tane();
    }
};
var func = document.getElementById("func");
func.onchange = () => {
    switch (func.value) {
        case "floor":
            display.innerText = String(Math.floor(parseInt(display.innerText)));
            break;
        case "abs":
            display.innerText = String(Math.abs(parseInt(display.innerText)));
            break;
        case "ceil":
            display.innerText = String(Math.ceil(parseInt(display.innerText)));
            break;
    }
};
function sine() {
    if (radtodeg) {
        display.innerText = String(Math.sin(Number(display.innerText)));
    }
    else {
        display.innerText = eval(String(Math.sin(Number(display.innerText)) * (Math.PI / 180)));
    }
}
function cosec() {
    if (radtodeg) {
        display.innerText = String(Math.cos(Number(display.innerText)));
    }
    else {
        display.innerText = eval(String(Math.cos(Number(display.innerText)) * (Math.PI / 180)));
    }
}
function tane() {
    if (radtodeg) {
        display.innerText = String(Math.tan(Number(display.innerText)));
    }
    else {
        display.innerText = eval(String(Math.tan(Number(display.innerText)) * (Math.PI / 180)));
    }
}
